<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        DB::table('users')->delete();
        DB::table('news')->delete();
        DB::table('colors')->delete();

        DB::table('users')->insert([
            'name' => 'Admin User',
            'email' => 'admin@noticiaspatito.com',
            'password' => bcrypt('admin2019'),
        ]);
        DB::table('users')->insert([
            'name' => 'Enrique Jiménez',
            'email' => 'enjimenez@noticiaspatito.com',
            'password' => bcrypt('enjimenez'),
        ]);

        DB::table('colors')->insert([
            'color' => 'Negro',
        ]);
        DB::table('colors')->insert([
            'color' => 'Café',
        ]);
        DB::table('colors')->insert([
            'color' => 'Azul',
        ]);
        DB::table('colors')->insert([
            'color' => 'Rojo',
        ]);
        DB::table('colors')->insert([
            'color' => 'Verde',
        ]);
        DB::table('colors')->insert([
            'color' => 'Amarillo',
        ]);
        DB::table('colors')->insert([
            'color' => 'Morado',
        ]);

        DB::table('categories')->insert([
            'category' => 'Tecnología',
        ]);
        DB::table('categories')->insert([
            'category' => 'Ciencia',
        ]);
        DB::table('categories')->insert([
            'category' => 'Salud',
        ]);
    }
}
