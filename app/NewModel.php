<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewModel extends Model 
{
    public $path = '/images/';
    protected $table = 'news';
    protected $primaryKey = 'id';
    protected $fillable = [
        'title',
        'content',
        'image',
        'color',
        'active'
    ];

    public function getImageAttribute($value){
        return $this->path . $value;
    }
    
}

