<?php

use App\NewModel;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


/*
|--------------------------------------------------------------------------
| Project Routes
|--------------------------------------------------------------------------
*/


// Route::auth();

Route::get('/login', 'CustomAuth@showLogin');
Route::post('login', 'CustomAuth@login');
Route::get('logout', 'CustomAuth@logout');

Route::get('/', 'HomeController@index');

//Route::resource('/news', 'NewsController');

Route::post('/news', 'NewsController@store')->name('news.store');
Route::get('/news', 'NewsController@index')->name('news.index');
Route::get('/news/category/{category}', 'NewsController@category')->name('news.category');
Route::get('/news/create', 'NewsController@create')->name('news.create');
Route::delete('/news/{news}', 'NewsController@destroy')->name('news.destroy');
Route::get('/news/{news}', 'NewsController@show')->name('news.show');
Route::patch('/news/{news}', 'NewsController@update')->name('news.update');
Route::get('/news/{news}/edit', 'NewsController@edit')->name('news.edit');

//Route::resource('/admin', 'AdminController');

Route::post('/admin', 'AdminController@store')->name('admin.store');
Route::get('/admin', 'AdminController@index')->name('admin.index');
Route::get('/admin/category/{category}', 'AdminController@category')->name('admin.category');
Route::get('/admin/create', 'AdminController@create')->name('admin.create');
Route::delete('/admin/{admin}', 'AdminController@destroy')->name('admin.destroy');
Route::get('/admin/{admin}', 'AdminController@show')->name('admin.show');
Route::patch('/admin/{admin}', 'AdminController@update')->name('admin.update');
Route::get('/admin/{admin}/edit', 'AdminController@edit')->name('admin.edit');