<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\NewModel;
use App\Http\Requests\StoreRequest;
use App\Http\Requests\UpdateRequest;

class NewsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = null;
        if ($request->has('search')) {
            $search = $request->input('search');
        }
        if ($search){
            $news = NewModel::where('title', 'like', '%'.$search.'%')->get()->sortByDesc("id");   
        } else {
            $news = NewModel::all()->sortByDesc("id");
        }
        foreach($news as $new){
            switch($new->color){
                case 1:
                    $new->parsed_color='black';
                    break;
                case 2:
                    $new->parsed_color='brown';
                    break;
                case 3:
                    $new->parsed_color='blue';
                    break;
                case 4:
                    $new->parsed_color='red';
                    break;
                case 5:
                    $new->parsed_color='green';
                    break;
                case 6:
                    $new->parsed_color='yellow';
                    break;
                case 7:
                    $new->parsed_color='purple';
                    break;
                default:
                    $new->parsed_color='black';
                    break;
            }
            switch($new->category){
                case 1:
                    $new->parsed_category='Tecnología';
                    break;
                case 2:
                    $new->parsed_category='Ciencia';
                    break;
                case 3:
                    $new->parsed_category='Salud';
                    break;
                default:
                    $new->parsed_category='Tecnología';
                    break;
            }
        }
        return view('news.index', compact('news'));
    }

    /**
     * Display a listing by category of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function category(Request $request, $category)
    {
        $search = null;
        if ($request->has('search')) {
            $search = $request->input('search');
        }
        $category_id;
        $category_name;
        switch($category){
            case 'technology':
                $category_id=1;
                $category_name='Tecnología';
                break;
            case 'science':
                $category_id=2;
                $category_name='Ciencia';
                break;
            case 'health':
                $category_id=3;
                $category_name='Salud';
                break;
            default:
                $category_id=1;
                $category_name='Tecnología';
                break;
        }
        if ($search){
            $news = NewModel::where('category', '=', $category_id)->where('title', 'like', '%'.$search.'%')->get()->sortByDesc("id");   
        } else {
            $news = NewModel::where('category', '=', $category_id)->get()->sortByDesc("id");
        }
        foreach($news as $new){
            switch($new->color){
                case 1:
                    $new->parsed_color='black';
                    break;
                case 2:
                    $new->parsed_color='brown';
                    break;
                case 3:
                    $new->parsed_color='blue';
                    break;
                case 4:
                    $new->parsed_color='red';
                    break;
                case 5:
                    $new->parsed_color='green';
                    break;
                case 6:
                    $new->parsed_color='yellow';
                    break;
                case 7:
                    $new->parsed_color='purple';
                    break;
                default:
                    $new->parsed_color='black';
                    break;
            }
            switch($new->category){
                case 1:
                    $new->parsed_category='Tecnología';
                    break;
                case 2:
                    $new->parsed_category='Ciencia';
                    break;
                case 3:
                    $new->parsed_category='Salud';
                    break;
                default:
                    $new->parsed_category='Tecnología';
                    break;
            }
        }
        return view('news.category', compact('news', 'category_name'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return redirect('/news');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        return redirect('/news');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $new = NewModel::findOrFail($id);
        switch($new->color){
            case 1:
                $new->parsed_color='black';
                break;
            case 2:
                $new->parsed_color='brown';
                break;
            case 3:
                $new->parsed_color='blue';
                break;
            case 4:
                $new->parsed_color='red';
                break;
            case 5:
                $new->parsed_color='green';
                break;
            case 6:
                $new->parsed_color='yellow';
                break;
            case 7:
                $new->parsed_color='purple';
                break;
            default:
                $new->parsed_color='black';
                break;
        }
        switch($new->category){
            case 1:
                $new->parsed_category='Tecnología';
                break;
            case 2:
                $new->parsed_category='Ciencia';
                break;
            case 3:
                $new->parsed_category='Salud';
                break;
            default:
                $new->parsed_category='Tecnología';
                break;
        }
        return view('news.show', compact('new'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return redirect('/news');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $id)
    {
        return redirect('/news');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return redirect('/news');
    }
}
