<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\NewModel;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news = NewModel::all()->sortByDesc("id")->take(3);
        foreach($news as $new){
            switch($new->color){
                case 1:
                    $new->parsed_color='black';
                    break;
                case 2:
                    $new->parsed_color='brown';
                    break;
                case 3:
                    $new->parsed_color='blue';
                    break;
                case 4:
                    $new->parsed_color='red';
                    break;
                case 5:
                    $new->parsed_color='green';
                    break;
                case 6:
                    $new->parsed_color='yellow';
                    break;
                case 7:
                    $new->parsed_color='purple';
                    break;
                default:
                    $new->parsed_color='black';
                    break;
            }
            switch($new->category){
                case 1:
                    $new->parsed_category='Tecnología';
                    break;
                case 2:
                    $new->parsed_category='Ciencia';
                    break;
                case 3:
                    $new->parsed_category='Salud';
                    break;
                default:
                    $new->parsed_category='Tecnología';
                    break;
            }
        }
        return view('home', compact('news'));
    }
}
