<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\User;
use Validator;
use App\Http\Controllers\Controller;


class CustomAuth extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLogin()
    {
        return view('auth.login');
    }

    public function login(Request $request)
    {
        //Get users data
        $filePath = getcwd().DIRECTORY_SEPARATOR."users.txt";
        if (file_exists($filePath)){
            $usersData = file_get_contents($filePath);
            $users = unserialize($usersData);   
            if (!empty($users)){
                $users_list = $users->list;
                while(count($users_list)>0) {
                    $user = array_pop($users_list);
                    if($request->email==$user->email){
                        if(password_verify($request->password, $user->password)){
                            $request->session()->put(['authenticated'=>true, 'name'=>$user->name, 'email'=>$user->email]);
                            return redirect('/admin');
                        } else {
                            return redirect()->back()->withErrors(['password' => 'Contraseña Incorrecta']);
                        }
                    }
                }
                return redirect()->back()->withErrors(['email' => 'Usuario no encontrado']);
            } else {
                return redirect()->back()->withErrors(['email' => 'Usuario no encontrado']);
            }
        } else {
            return redirect()->back()->withErrors(['email' => 'Usuario no encontrado']);
        }
    }

    public function logout(Request $request)
    {
        $request->session()->flush();
        return redirect('/');
    }

}
