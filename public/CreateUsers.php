<?php

$adminUser = new stdClass();
$adminUser->name = "Admin User";
$adminUser->email = "admin@noticiaspatito.com";
$adminUser->password = password_hash("admin2019", PASSWORD_BCRYPT);

$enjimenezUser = new stdClass();
$enjimenezUser->name = "Enrique Jiménez";
$enjimenezUser->email = "enjimenez@noticiaspatito.com";
$enjimenezUser->password = password_hash("enjimenez", PASSWORD_BCRYPT);

$users = new stdClass();
$users->list = [$adminUser,$enjimenezUser];

$usersData = serialize($users);
$filePath = getcwd().DIRECTORY_SEPARATOR."users.txt";
if (is_writable($filePath)) {
    $fp = fopen($filePath, "w"); 
    fwrite($fp, $usersData); 
    fclose($fp);
}

?>

