@extends('layouts.app')

@section('content')

    <div class="container"> 
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="row" style="text-align:center">
                    {!! Form::open(['url'=>Request::url(), 'method'=>'GET', 'class'=>'form-horizontal']) !!}
                        <div class="form-group">
                            {!! Form::label('search', 'Buscar noticias:', ['class'=>'col-md-4 control-label']) !!}
                            <div class="col-md-5">
                                {!! Form::text('search', null, ['class'=>'form-control']) !!}
                            </div>
                            <div class="col-md-1">
                                {!! Form::submit('Buscar', ['class'=>'btn btn-primary']) !!}
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>

        @foreach($news->chunk(3) as $chunk)
            <div class="row">
                @foreach($chunk as $new)
                    <div class="col-md-4">
                        
                            <div class="panel panel-default" style="text-align:center">
                                <div class="panel-heading" onclick="location.href='{{ route('admin.show', $new->id) }}'"><h4>{{$new->title}}</h4><h5>{{$new->parsed_category}}</h5></div>

                                <div class="panel-body">
                                    <div class="row" onclick="location.href='{{ route('admin.show', $new->id) }}'">
                                        <div class="image-container"><img src="{{$new->image}}" alt="" style="max-width:100%; max-height:200px"></div>
                                    </div>
                                    <br>
                                    <div class="row" style="color:{{$new->parsed_color}}; text-align:left; padding-left:15px; padding-right:15px;" onclick="location.href='{{ route('admin.show', $new->id) }}'">
                                        {!! substr( nl2br(e($new->content)) ,0,255) . "..." !!}
                                    </div>
                                    <br>
                                    <div class="row">
                                        {!! Form::open(['method'=>'DELETE', 'action'=>['AdminController@destroy', $new->id]]) !!}
                                            <button type="button" onclick="location.href='{{ route('admin.show', $new->id) }}'" class="btn btn-primary">Ver</button>
                                            <button type="button" onclick="location.href='{{ route('admin.edit', $new->id) }}'" class="btn btn-warning">Editar</button>
                                            {!! Form::submit('Borrar', ['class'=>'btn btn-danger']) !!}
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>
                    </div>
                @endforeach
            </div>
        @endforeach
           
        <br>
        <div class="row" style="text-align:center">
            <button type="button" onclick="location.href='{{ route('admin.create') }}'" class="btn btn-primary btn-lg">Nueva Noticia</button>
        </div>
        <br>
    </div>

@endsection