@extends('layouts.app')

@section('content')

<div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Editar Noticia</div>
                    <div class="panel-body">

                        {!! Form::model($new, ['method'=>'PATCH', 'action'=>['AdminController@update', $new->id], 'files'=>true, 'class'=>'form-horizontal']) !!}

                        <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                            {!! Form::label('title', 'Título:', ['class'=>'col-md-4 control-label']) !!}
                            <div class="col-md-6">
                                {!! Form::text('title', null, ['class'=>'form-control']) !!}
                                @if ($errors->has('title'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('content') ? ' has-error' : '' }}">
                            {!! Form::label('content', 'Contenido:', ['class'=>'col-md-4 control-label']) !!}
                            <div class="col-md-6">
                                {!! Form::textarea('content', null, ['class'=>'form-control']) !!}
                                @if ($errors->has('content'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('content') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('category') ? ' has-error' : '' }}">
                            {!! Form::label('content', 'Categoría:', ['class'=>'col-md-4 control-label']) !!}
                            <div class="col-md-6">
                                {!! Form::select('category', $categories, null, ['class'=>'form-control']) !!}
                                @if ($errors->has('category'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('category') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('color') ? ' has-error' : '' }}">
                            {!! Form::label('content', 'Color:', ['class'=>'col-md-4 control-label']) !!}
                            <div class="col-md-6">
                                {!! Form::select('color', $colors, null, ['class'=>'form-control']) !!}
                                @if ($errors->has('color'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('color') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                            {!! Form::label('image', 'Imagen:', ['class'=>'col-md-4 control-label']) !!}
                            <div class="col-md-6">
                                {!! Form::file('image', null, ['class'=>'form-control']) !!}
                                @if ($errors->has('image'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('image') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-6" style="text-align:right">
                            {!! Form::submit('Editar', ['class'=>'btn btn-primary']) !!}
                        </div>

                        {!! Form::close() !!}



                        {!! Form::open(['method'=>'DELETE', 'action'=>['AdminController@destroy', $new->id]]) !!}

                        <div class="col-md-6" style="text-align:left">
                            {!! Form::submit('Borrar', ['class'=>'btn btn-danger']) !!}
                        </div>
                        {!! Form::close() !!}

                        
                    </div>
                </div>
            </div>
        </div>
    </div>



                    


@endsection