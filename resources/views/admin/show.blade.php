@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="panel panel-default" style="text-align:center">
                <div class="panel-heading"><h4>{{$new->title}}</h4><h5>{{$new->parsed_category}}</h5></div>

                <div class="panel-body">
                    <div class="row">
                        <div class="image-container"><img src="{{$new->image}}" alt="" style="max-width:100%; max-height:400px"></div>
                    </div>
                    <br>
                    <div class="row" style="color:{{$new->parsed_color}}; text-align:left; padding-left:15px; padding-right:15px;">
                        {!! nl2br(e($new->content)) !!}
                    </div>
                </div>
            </div>
        </div>
        <div class="row" style="text-align:center">
            <button type="button" onclick="location.href='{{ route('admin.edit', $new->id) }}'" class="btn btn-warning btn-lg">Editar</button>
            <button type="button" onclick="location.href='{{ route('admin.index') }}'" class="btn btn-primary btn-lg">Regresar</button>
        </div>
    </div>
    
@endsection