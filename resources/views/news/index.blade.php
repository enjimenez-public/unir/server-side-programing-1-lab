@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="row" style="text-align:center">
                {!! Form::open(['url'=>Request::url(), 'method'=>'GET', 'class'=>'form-horizontal']) !!}
                    <div class="form-group">
                        {!! Form::label('search', 'Buscar noticias:', ['class'=>'col-md-4 control-label']) !!}
                        <div class="col-md-5">
                            {!! Form::text('search', null, ['class'=>'form-control']) !!}
                        </div>
                        <div class="col-md-1">
                            {!! Form::submit('Buscar', ['class'=>'btn btn-primary']) !!}
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">

                @foreach($news as $new)
                    <div class="panel panel-default" style="text-align:center">
                        <div class="panel-heading" onclick="location.href='{{ route('news.show', $new->id) }}'"><h4>{{$new->title}}</h4><h5>{{$new->parsed_category}}</h5></div>

                        <div class="panel-body">
                            <div class="row" onclick="location.href='{{ route('news.show', $new->id) }}'">
                                <div class="image-container"><img src="{{$new->image}}" alt="" style="max-width:100%; max-height:400px"></div>
                            </div>
                            <br>
                            <div class="row" style="color:{{$new->parsed_color}}; text-align:left; padding-left:15px; padding-right:15px;" onclick="location.href='{{ route('news.show', $new->id) }}'">
                                {!! substr( nl2br(e($new->content)) ,0,511) . "..." !!}
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>

@endsection