@extends('layouts.app')

@section('content')

    <h1>Editar Noticia</h1>
    
    {!! Form::model($new, ['method'=>'PATCH', 'action'=>['NewsController@update', $new->id], 'files'=>true]) !!}

    <div class="form-group">
        {!! Form::label('title', 'Título:') !!}
        {!! Form::text('title', null, ['class'=>'form-controll']) !!}

        {!! Form::label('content', 'Contenido:') !!}
        {!! Form::textarea('content', null, ['class'=>'form-controll']) !!}
    
    </div>

    <div class="form-group">
        {!! Form::label('image', 'Imagen:') !!}
        {!! Form::file('image', null, ['class'=>'form-controll']) !!}
    </div>

    <div class="form-group">
        {!! Form::submit('Editar', ['class'=>'btn btn-primary']) !!}
    </div>

    {!! Form::close() !!}


    {!! Form::open(['method'=>'DELETE', 'action'=>['NewsController@destroy', $new->id]]) !!}

    <div class="form-group">
        {!! Form::submit('Borrar', ['class'=>'btn btn-danger']) !!}
    </div>

    {!! Form::close() !!}

    @if(count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
            </ul>
        </div>
    @endif

@endsection