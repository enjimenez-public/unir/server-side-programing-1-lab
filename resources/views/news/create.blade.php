@extends('layouts.app')

@section('content')

    <h1>Nueva Noticia</h1>

    {!! Form::open(['method'=>'POST', 'action'=>'NewsController@store', 'files'=>true]) !!}

    <div class="form-group">
        {!! Form::label('title', 'Título:') !!}
        {!! Form::text('title', null, ['class'=>'form-controll']) !!}

        {!! Form::label('content', 'Contenido:') !!}
        {!! Form::textarea('content', null, ['class'=>'form-controll']) !!}
    
    </div>

    <div class="form-group">
        {!! Form::label('image', 'Imagen:') !!}
        {!! Form::file('image', null, ['class'=>'form-controll']) !!}
    </div>

    <div class="form-group">
        {!! Form::submit('Crear', ['class'=>'btn btn-primary']) !!}
    </div>

    {!! Form::close() !!}

    @if(count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
            </ul>
        </div>
    @endif

@endsection
