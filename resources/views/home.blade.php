@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row" style="text-align:center">
        <h1>Lo más reciente</h1>
    </div>
    <br>
    <div class="row">
            
        @foreach($news as $new)
            <a href="{{route('news.show', $new->id)}}">
                <div class="col-md-4 col-md-offset-0">
                    <div class="panel panel-default" style="text-align:center">
                        <div class="panel-heading"><h4>{{$new->title}}</h4><h5>{{$new->parsed_category}}</h5></div>

                        <div class="panel-body">
                            <div class="row">
                                <div class="image-container"><img src="{{$new->image}}" alt="" style="max-width:100%; max-height:400px"></div>
                            </div>
                            <br>
                            <div class="row" style="color:{{$new->parsed_color}}; text-align:left; padding-left:15px; padding-right:15px;">
                                {!! substr( nl2br(e($new->content)),0,511) . "..." !!}
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        @endforeach

    </div>
    <br>
    <div class="row" style="text-align:center">
        <button type="button" onclick="location.href='{{ route('news.index') }}'" class="btn btn-primary btn-lg">Ver todas las noticias</button>
    </div>
</div>
@endsection
